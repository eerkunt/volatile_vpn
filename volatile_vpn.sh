#!/usr/bin/env bash

set -o pipefail
set -e

cd terraform

echo "Volatile VPN v0.0.1 ALPHA"
echo -n "Removing the old infrastructure"

terraform destroy -force | while read LINE
do
    echo -n "."
done
echo ""
echo -n "Creating the infrastructure"

echo yes | terraform apply | while read LINE
do
    echo -n "."
done

echo ""
echo "Infrastructure creation completed."
sleep 10

echo -n "Provisioning started for OpenVPN and side services."
ssh -n -i ~/.ssh/keys/private/emre-volatile-vpn.pem \
               -oStrictHostKeyChecking=no ubuntu@$(terraform output volatile_vpn.public_ip) \
               tail -n 100 -f /tmp/part-001.log | while read LINE
do
    echo -n '.'
    if [[ $LINE == '**** INSTALL FINISHED ****' ]]; then
        echo ""
        echo "Installation Finished."
        echo -n "Creating user."
    elif [[ $LINE == '**** USERS CREATED ****' ]]; then
        echo ""
        echo "User created"
        echo "Downloading related files"
        scp -r -i ~/.ssh/keys/private/emre-volatile-vpn.pem \
            -oStrictHostKeyChecking=no \
            ubuntu@$(terraform output volatile_vpn.public_ip):/var/local/src/instance-bootstrap/fetched_creds .
        ls -al
    fi
done

echo ""
echo "All done."
