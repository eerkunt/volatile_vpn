terraform {
  required_version = "0.11.1"
}

provider "aws" {
  region = "eu-west-2"
  shared_credentials_file = "~/.aws/credentials"
}

data "template_file" "userdata" {
  template = "${file("${path.module}/user_data.sh")}"
}

resource "aws_lightsail_instance" "volatile_vpn" {
  name              = "vol-vpn"
  availability_zone = "eu-west-2a"
  blueprint_id      = "ubuntu_16_04_1"
  bundle_id         = "nano_1_0"
  key_pair_name     = "emre-volatile-vpn"
  user_data         = "${data.template_file.userdata.rendered}"
}

output "volatile_vpn.public_ip" {
  value = "${aws_lightsail_instance.volatile_vpn.public_ip_address}"
}