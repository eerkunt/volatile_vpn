#!/bin/bash -x
set -e
set -x

exec > /tmp/part-001.log 2>&1

sudo apt -y update \
&& sudo apt -y install git python ansible

sudo ssh-keygen -t rsa -b 2048 -N "" -f /root/.ssh/id_rsa

ANSIBLE_ROOT_DIR=/var/local/src/instance-bootstrap
ansible-pull --accept-host-key --verbose --url 'https://bitbucket.org/eerkunt/volatile_vpn.git' \
             --directory $ANSIBLE_ROOT_DIR -i ansible/inventory ansible/install.yml

echo -n "\r\n\r\n"
echo '**** INSTALL FINISHED ****'

ansible-playbook $ANSIBLE_ROOT_DIR/ansible/add_clients.yml \
        -i $ANSIBLE_ROOT_DIR/ansible/inventory \
        -e clients_to_add=sharky

echo -n "\r\n\r\n"
echo '**** USERS CREATED ****'
echo ""
